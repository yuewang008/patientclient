package com.sixplus.med_sys_patient;

public class MedConstants {
    public static final String IMAGE_STORAGE_DIR = "medsixplus"; 
    public static final boolean DEBUG = true;
    
    public static final String DBNAME = "pat_sixplus_db";
    
//    public static final String SERVER_URL = "http://192.168.1.90/vtiger_temp/";
    public static final String SERVER_URL = "http://54.214.27.29/vtigercrm/";
    public static final String PHP_API_URL = "modules/Mobile/api.php";
    public static final String UPLOAD_IMAGE_URL = "modules/Mobile/api/ws/Med_image_receive.php";
    public static final String UPLOAD_AUDIO_URL = "modules/Mobile/api/ws/Med_audio_receive.php";

    public static final String DOWNLOAD_IMAGE_URL = "medsixplus/image/";
    public static final String DOWNLOAD_AUDIO_URL = "medsixplus/audio/";
    public static final String NO_ATTACH_URL = "";

    public static final int NO_ATTACH = 0;
    public static final int AUDIO_TYPE = 1;
    public static final int IMAGE_TYPE = 2;
    
    public static final String[] phpHandlerArray = {NO_ATTACH_URL, UPLOAD_AUDIO_URL, UPLOAD_IMAGE_URL};
    public static final String[] downloadUrlArray = {NO_ATTACH_URL, DOWNLOAD_AUDIO_URL, DOWNLOAD_IMAGE_URL};
    
    public static final int PREVIEW = 0;
    public static final int ORIGINAL = 1;
}