package com.sixplus.med_sys_patient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sixplus.med_sys_patient.R;

import us.xdroid.util.xHelper;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MedQuestionList extends Activity implements OnItemClickListener {
	private static final String TAG = "MedQuestionList";
    // Progress Dialog
    private ProgressDialog pDialog;
    private int mPatID = 0;
    private int mDocID = 0;
    private List<QuestionInfo> mQuesList = null;
    private ListView mQuesListView = null;
    private TextView mFindNoQuesTextView = null;
    private Map<Integer, Integer> mQuestionMap = new HashMap<Integer, Integer>();
    SQLiteDatabase db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.med_question_list);
        
        mQuesListView = (ListView) findViewById(R.id.questionlistView);
        mQuesListView.setOnItemClickListener(this);
        
        mFindNoQuesTextView = (TextView)findViewById(R.id.find_no_question);
        mFindNoQuesTextView.setVisibility(View.GONE);

		TextView tx=(TextView)findViewById(R.id.ask_problems);
        tx.setClickable(true);
        tx.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(MedQuestionList.this, MedBringupQuestion.class);
				startActivity(intent);
			}
		});
        
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.title_activity_med_question_list);

        mPatID = ((MedPatientApp)getApplicationContext()).getPatientID();
        mDocID = ((MedPatientApp)getApplicationContext()).getDocID();

        db = openOrCreateDatabase(MedConstants.DBNAME, Context.MODE_PRIVATE, null);
        initLocalQuestionData();
        
        //Must init the MedController here!!!
  	  	if(MedController.mController == null) {
  	  		MedController controller = new MedController(this);
  	  	}

		String processing = getResources().getString(R.string.processing);
		//show progress dialog
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(processing);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
    }

    private void initLocalQuestionData() {
    	Cursor cr = null;
		
    	db.execSQL("CREATE TABLE IF NOT EXISTS question (question_id INTEGER, seq_id INTEGER, pat_id INTEGER, doc_id INTEGER)");

		String sql = "SELECT seq_id, question_id FROM question WHERE pat_id=? AND doc_id=?";
		cr = db.rawQuery(sql, new String[]{mPatID + "", mDocID + ""});
        while(cr.moveToNext()){
            int questionID = cr.getInt(cr.getColumnIndex("question_id"));
            int seqID = cr.getInt(cr.getColumnIndex("seq_id"));
            mQuestionMap.put(Integer.valueOf(questionID), Integer.valueOf(seqID));
        }
        cr.close();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	mPatID = ((MedPatientApp)getApplicationContext()).getPatientID();
    	mDocID = ((MedPatientApp)getApplicationContext()).getDocID();
  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);
    	//do the login in background
    	MedController.findQuestionList(mPatID, mDocID);
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	db.close();
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		QuestionInfo questionInfo = mQuesList.get((int)id);
		Intent intent = new Intent();
		intent.setClass(MedQuestionList.this, MedBringupQuestion.class);
		intent.putExtra("question_id", questionInfo.mQuestionID);
		startActivity(intent);
		if (questionInfo.mUpdated) {
        	updateQuestion(questionInfo.mQuestionID, questionInfo.mMaxSeqID);
		}
	}

    private List<Map<String, Object>> getData(JSONArray array) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        mQuesList = new ArrayList<QuestionInfo>();

        int length = array.length();
        for(int i=0;i<length;i++){
        	try {
                JSONObject object = array.getJSONObject(i);
                Map<String, Object> map = new HashMap<String, Object>();
                int question_id = object.getInt("question_id");
                int seq_id = object.getInt("max_seq_id");
                String createDate = object.getString("create_date");
                map.put("question_id", question_id);
                map.put("create_date", createDate);
                String updateString = "";
                boolean updated = isUpdated(question_id, seq_id);
                if (updated) {
                	updateString = this.getString(R.string.question_updated);
                }
                map.put("update", updateString);
				QuestionInfo questionInfoInstance = new QuestionInfo(question_id, seq_id, updated);
				mQuesList.add(questionInfoInstance);
	            list.add(map);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return list;
    }

    private void updateQuestion(int questionID, int seqID) {
    	String sql;
    	Cursor cr = null;
    	Integer localSeqID = (Integer)mQuestionMap.get(Integer.valueOf(questionID));
    	if (localSeqID == null) {
    		// the question has not been downloaded yet.
    		sql = "INSERT INTO question (seq_id, question_id, pat_id, doc_id) VALUES(?,?,?,?)";
    		cr = db.rawQuery(sql, new String[]{seqID + "", questionID + "", mPatID + "", mDocID + ""});
    		if (cr == null || cr.getCount() <= 0) Log.e(TAG, "insert message info sqlite failed!");
    	} else if (seqID > localSeqID.intValue()) {
    		sql = "UPDATE question SET seq_id=? WHERE question_id=?";
    		cr = db.rawQuery(sql, new String[]{String.valueOf(seqID), String.valueOf(questionID)});
    		if (cr == null || cr.getCount() <= 0) Log.e(TAG, "update message info sqlite failed!");
    	}
    	mQuestionMap.put(Integer.valueOf(questionID), Integer.valueOf(seqID));
    }
    
    private boolean isUpdated(int questionID, int seqID) {
    	Integer localSeqID = (Integer)mQuestionMap.get(Integer.valueOf(questionID));
    	if (localSeqID == null || seqID > localSeqID.intValue()) {
    		return true;
    	}
    	return false;
    }
    
    //implement the UI handler
	Handler mUiHandler = new Handler() {
      @Override
      public void handleMessage(android.os.Message msg) {
          switch (msg.what) {
			case  MedController.QUESTION_LIST:
                //Medcontroller will sent this message back after it handled the request
				Log.d(TAG, "msg.arg1 = " + msg.arg1);
				if(msg.arg1 == 0) {
					if (msg.obj != null) {
						try{
							JSONObject jso = (JSONObject)msg.obj;
		            		if(jso.getBoolean("success")) {
		            			Log.d(TAG, "success");
		            			//result array with the returned rows 
		            			JSONArray result = jso.getJSONArray("result");
								SimpleAdapter adapter = new SimpleAdapter(MedQuestionList.this,getData(result),R.layout.med_question_list_view,
						                new String[]{"question_id", "create_date", "update"},
						                new int[]{R.id.name, R.id.create_date, R.id.update});
								mQuesListView.setAdapter(adapter);
								adapter.notifyDataSetChanged();
								mQuesListView.smoothScrollToPosition(mQuesList.size());
								mFindNoQuesTextView.setVisibility(View.GONE);
		            		}
		            		else {
		            			mFindNoQuesTextView.setVisibility(View.VISIBLE);
		            		}
		                    xHelper.log(jso.toString());
		            	} catch (Exception e) {
		            		e.printStackTrace();
		            	}
					}
				} else {
					Toast.makeText(MedQuestionList.this, R.string.find_ques_list_failed, Toast.LENGTH_LONG).show();
				}
				pDialog.dismiss();
              break;
		  }
	   }
	};

	class QuestionInfo {
		public int mQuestionID;
		public int mMaxSeqID;
		public boolean mUpdated;
		public QuestionInfo(int ques_id, int seq_id, boolean updated) {
			mQuestionID = ques_id;
			mMaxSeqID = seq_id;
			mUpdated = updated;
		}
	}
}
