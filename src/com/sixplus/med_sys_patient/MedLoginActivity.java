package com.sixplus.med_sys_patient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sixplus.med_sys_patient.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MedLoginActivity extends Activity implements View.OnClickListener{
	private static final String TAG = "MedLoginActivity";
    // Progress Dialog
    private ProgressDialog pDialog;
    private TextView tv_head;
    EditText mUsernameText;
    EditText mPasswordText;
    Button mButtonLogin;
    Context mContext = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.login_title);
        
        //Must init the MedController here!!!
  	  	if(MedController.mController == null) {
  	  		MedController controller = new MedController(this);
  	  	}
  	  	mContext = this;
  	  	
  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);
  	  
        // Edit Text
        mUsernameText = (EditText) findViewById(R.id.username);
        mPasswordText = (EditText) findViewById(R.id.password);
        
		TextView button_login = (TextView)findViewById(R.id.button_login);
		button_login.setClickable(true);
		button_login.setOnClickListener(this);

		TextView button_register = (TextView)findViewById(R.id.button_register);
		button_register.setClickable(true);
		button_register.setOnClickListener(this);
    }

    public void onClick(View view) {
    	switch(view.getId()) {
    	case R.id.button_login:
        	String username = mUsernameText.getText().toString().trim();
        	String password = mPasswordText.getText().toString().trim();

        	if(!validate(username, password))
        		return;

        	//do the login in background
        	MedController.login(username, password);

    		String processing = getResources().getString(R.string.processing);
    		//show progress dialog
    		pDialog = new ProgressDialog(MedLoginActivity.this);
    		pDialog.setMessage(processing);
    		pDialog.setIndeterminate(false);
    		pDialog.setCancelable(true);
    		pDialog.show();
    		break;
    	case R.id.button_register:
            // Create Intent for MedRegisterActivity  abd Start The Activity
            Intent intentSignUP = new Intent(MedLoginActivity.this, MedRegisterActivity.class);
            startActivity(intentSignUP);
            finish();
    		break;
    	default:
    		break;
    	}

    }

    private boolean validate(String username, String password)
    {
        if (username.equals("") || password.equals(""))
        {
        	Toast.makeText(MedLoginActivity.this, R.string.emptynameorpassword, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	MedController.setUIHandler(mUiHandler);
    }

    //implement the UI handler
	Handler mUiHandler = new Handler() {
      @Override
      public void handleMessage(android.os.Message msg) {
    	  boolean loginSuccess = false;
          switch (msg.what) {
				case  MedController.LOGIN:
                    //Medcontroller will sent this message back after it handled the request
					Log.d(TAG, "msg.arg1 = " + msg.arg1);
					if(msg.arg1 == 0) {
	                    JSONObject jso = (JSONObject)msg.obj;
	                    if (jso != null) {
	                    	try{
	                    		if(jso.getBoolean("success")) {
	                    			JSONObject result = jso.getJSONObject("result");
	                    			if (result != null) {
	                    				int patID = result.getInt("pat_id");
	                    				((MedPatientApp)getApplicationContext()).setPatientID(patID);
	                    			}
	                    			loginSuccess = true;
	                    		}
	                    	} catch (JSONException e1) {
					            // TODO Auto-generated catch block
					        	e1.printStackTrace();
					            return;
					        }
	                    }
	                    if (loginSuccess) {
							Intent intent = new Intent();
							intent.setClass(mContext, DocListActivity.class);
							startActivity(intent);
							finish();
	                    } else {
							Toast.makeText(MedLoginActivity.this, R.string.login_failed, Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(MedLoginActivity.this, R.string.login_failed, Toast.LENGTH_LONG).show();
					}
					pDialog.dismiss();
                  break;
			case  MedController.REGISTER:
                    //Medcontroller will sent this message back after it handled the request
					Log.d(TAG, "msg.arg1 = " + msg.arg1);
					if(msg.arg1 == 0) {
						try {
							JSONArray patient_array = (JSONArray) msg.obj;
							JSONObject patient_info = patient_array.getJSONObject(0);
							if (patient_info != null) {
								int patient_id = patient_info.getInt("pat_id");
								((MedPatientApp)getApplicationContext()).setPatientID(patient_id);
								Log.d(TAG, "patient id = " + patient_info.getInt("pat_id"));
								Log.d(TAG, "patient name = " + patient_info.getString("name"));
							}
				        } catch (JSONException e1) {
				            // TODO Auto-generated catch block
				        	e1.printStackTrace();
				            return;
				        }
						Intent intent = new Intent();
						intent.setClass(mContext, DocListActivity.class);
						mContext.startActivity(intent);
						finish();
						Toast.makeText(MedLoginActivity.this, "Register success", Toast.LENGTH_LONG).show();
					}
					else
						Toast.makeText(MedLoginActivity.this, "Register failed!", Toast.LENGTH_LONG).show();
					pDialog.dismiss();
                  break;  
		  }
	   }
	};
}
