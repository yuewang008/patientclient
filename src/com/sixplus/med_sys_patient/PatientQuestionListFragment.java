package com.sixplus.med_sys_patient;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import com.sixplus.med_sys_patient.R;

import us.xdroid.util.xHelper;

public class PatientQuestionListFragment extends Fragment
    implements AdapterView.OnItemClickListener
{
    private final static String TAG = "sixplusPatientQuestionListFragment";
    private final static boolean DEBUG = MedConstants.DEBUG;
    private ListView listView;
    private ProgressDialog mProgressDialog;
    private QuestionListAdapter listAdapter;
    private ArrayList<PatientQuestion> mPatQuestionList = null;
    private ViewClickListener viewClickListener = null;

    public void onActivityCreated(Bundle paramBundle)
    {
    	if (DEBUG) Log.d(TAG, "onActivityCreated()");
        super.onActivityCreated(paramBundle);
    }

    public void update() {
        if (DEBUG) Log.d(TAG, "mPatQuestionList length = " + mPatQuestionList.size());
        this.listAdapter.notifyDataSetChanged();
        listView.smoothScrollToPosition(mPatQuestionList.size());
    }

    public void setList(ArrayList<PatientQuestion> list) {
        if (DEBUG) Log.d(TAG, "setList called");
        mPatQuestionList = list;
        if (DEBUG) Log.d(TAG, "mPatQuestionList length = " + mPatQuestionList.size());
        this.listAdapter.notifyDataSetChanged();
    }

    private ProgressDialog createProgressDialog() {
          ProgressDialog dialog = new ProgressDialog(this.getActivity());
          dialog.setCancelable(true);
          dialog.setTitle("����");
          dialog.setMessage("���ڲ��ң����Ժ�");
          return dialog;
    }

    private void onGetPatientQuestionListDone(int arg) {
        mProgressDialog.dismiss();
        this.listAdapter.notifyDataSetChanged();
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
    	if (DEBUG) Log.d(TAG, "onCreateView()");
          super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);

          listView= new ListView(getActivity());
          this.listView.setOnItemClickListener(this);

          this.listAdapter = new QuestionListAdapter(getActivity());
          listView.setAdapter(listAdapter);

          listView.setCacheColorHint(0);
          return this.listView;
    }

    void doRetrieve(){

    }

    private View.OnClickListener imgPreviewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
      	  viewClickListener.onViewClick(arg0);
        }
    };

    private View.OnClickListener wavIconClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
      	  viewClickListener.onViewClick(arg0);
        }
    };

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        /*
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setClass(this.getActivity(),  PatientAskedQuestionActivity.class);
        intent.putExtra("patient_index", paramInt-1);
        startActivity(intent);
        */

    }

    public void onPause()
    {
      super.onPause();
    }

    public void onResume()
    {
      super.onResume();
    }

    class QuestionListAdapter extends BaseAdapter{
        Context context;

        public QuestionListAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
        	if (DEBUG) Log.d(TAG, "getCount()");
            if (mPatQuestionList == null) {
            	if (DEBUG) Log.d(TAG, "getCount() return 0");
                return 0;
            }
            if (DEBUG) Log.d(TAG, "getCount() return:" + mPatQuestionList.size());
            return mPatQuestionList.size();
        }

        @Override
        public long getItemId(int position) {
        	if (DEBUG) Log.d(TAG, "getItemId()");
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (DEBUG) Log.d(TAG, "getView(). position = " + position);
            LayoutInflater inflater = LayoutInflater.from(context);
//            View localView = null;
//            if (convertView != null) {
//            	localView = convertView;
//            }
            View localView = inflater.inflate(R.layout.med_patient_ask_question_item, null);

              PatientQuestion patQues = getItem(position);

              TextView tvLeftMsg = (TextView) localView.findViewById(R.id.left_message);
              TextView tvRightMsg = (TextView) localView.findViewById(R.id.right_message);
              ImageView tvLeftIcon = (ImageView) localView.findViewById(R.id.left_icon);
              ImageView tvRightIcon = (ImageView) localView.findViewById(R.id.right_icon);
              ImageView tvLeftImgPreview = (ImageView) localView.findViewById(R.id.left_img_preview);
              ImageView tvLeftWavIcon = (ImageView) localView.findViewById(R.id.left_wav_icon);

              if(patQues.mQuestionDirection == 0) {
                  if (DEBUG) Log.d(TAG, "patQues.mQuestionDirection == 0");
                  tvLeftMsg.setVisibility((patQues.mQuestionText == null || patQues.mQuestionText.trim().equals(""))? View.GONE : View.VISIBLE);
                  tvLeftMsg.setText((patQues.mQuestionText == null || patQues.mQuestionText.trim().equals(""))? "" : patQues.mQuestionText);
                  tvLeftImgPreview.setVisibility((patQues.mQuestionPictureUrl == null || patQues.mQuestionPictureUrl.trim().equals(""))? View.GONE : View.VISIBLE);
                  tvLeftWavIcon.setVisibility((patQues.mQuestionWavUrl == null || patQues.mQuestionWavUrl.trim().equals(""))? View.GONE : View.VISIBLE);

                  if (DEBUG) Log.d(TAG, "patQues.mQuestionText " + patQues.mQuestionText);
                  if (patQues.mQuestionText != null && !patQues.mQuestionText.trim().equals("")) {
                      tvLeftMsg.setText(patQues.mQuestionText);
                  }

                  if(patQues.mQuestionPictureUrl != null && patQues.mQuestionPicturePreviewUrl != null) {
                      tvLeftImgPreview.setOnClickListener(imgPreviewClickListener);
                      if (DEBUG) Log.d(TAG, "patQues.mQuestionPictureUrl " + patQues.mQuestionPictureUrl);
                      if (DEBUG) Log.d(TAG, "patQues.mQuestionPicturePreviewUrl " + patQues.mQuestionPicturePreviewUrl);
                      Bitmap image = BitmapFactory.decodeFile(patQues.mQuestionPicturePreviewUrl);
                      tvLeftImgPreview.setImageBitmap(image);
                      tvLeftImgPreview.setTag(new QuesViewInfo(MedConstants.IMAGE_TYPE, patQues.mQuestionPictureUrl));
                  }

                  if(patQues.mQuestionWavUrl != null) {
                      tvLeftWavIcon.setOnClickListener(wavIconClickListener);
                      tvLeftWavIcon.setTag(new QuesViewInfo(MedConstants.AUDIO_TYPE, patQues.mQuestionWavUrl));
                  }

                  tvRightMsg.setVisibility(View.GONE);
                  tvRightIcon.setVisibility(View.GONE);
              } else {
                  tvRightMsg.setText(patQues.mQuestionText);
                  tvLeftMsg.setVisibility(View.GONE);
                  tvLeftIcon.setVisibility(View.GONE);
                  tvLeftImgPreview.setVisibility(View.GONE);
                  tvLeftWavIcon.setVisibility(View.GONE);
              }
            return localView;
        }

        @Override
        public PatientQuestion getItem(int arg0) {
            // TODO Auto-generated method stub
            xHelper.log("get " + arg0 + " : " + mPatQuestionList.get(arg0).toString());
            return mPatQuestionList.get(arg0);
        }
    }

    interface ViewClickListener {
    	public void onViewClick(View view);
    }

    public void setOnViewClickListener(ViewClickListener listener) {
    	viewClickListener = listener;
    }
}