package com.sixplus.med_sys_patient;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

public class MedViewPhotoActivity extends Activity {
	private ImageView mImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.med_view_photo);
		
		((TextView)findViewById(R.id.navigation_title)).setText(R.string.view_image);
		
		mImageView = (ImageView) findViewById(R.id.image_view);
		
		String imageUrl = getIntent().getStringExtra("imagePath");
		if (imageUrl != null && !imageUrl.trim().equals("")) {
            
            Bitmap image = BitmapFactory.decodeFile(imageUrl);
            mImageView.setImageBitmap(image);
		}
	}
	
	
}
