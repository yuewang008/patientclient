package com.sixplus.med_sys_patient;

import java.io.File;

import android.os.Environment;
import android.util.Log;

public class MedUtil {
    static final String TAG = "MedUtil";
    final static boolean DEBUG = MedConstants.DEBUG;

	static String saveDir = null;
	static String saveImageDir = null;
	static String saveAudioDir = null;

    public static boolean createDir(String dirPath) {
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        if (!dir.exists()) {
            Log.d(TAG, "create folder failed");
            return false;
        }
        return true;
    }

    public static boolean initialFolders() {
        String state = Environment.getExternalStorageState();
        
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            saveDir = Environment.getExternalStorageDirectory()
                    + File.separator + "medsixplus" + File.separator;
            saveImageDir = saveDir + "image" + File.separator;
            saveAudioDir = saveDir + "audio" + File.separator;
            if (!createDir(saveDir)) return false;
            if (!createDir(saveImageDir)) return false;
            if (!createDir(saveAudioDir)) return false;
        }
        return true;
    }
    
    public static String getFileName(String url) {
    	String filename = null;
    	if (url != null) {
    		filename = url.substring(url.lastIndexOf("/") + 1);
    	}
    	return filename;
    }
}
