package com.sixplus.med_sys_patient;

import java.sql.Date;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import android.widget.DatePicker;

import android.widget.Spinner;


@SuppressLint("NewApi")
public class MedRegisterActivity extends Activity implements View.OnClickListener
{
	private static final String TAG = "MedRegisterActivity";
	// Progress Dialog
	private ProgressDialog pDialog;
	EditText editTextUserName,editTextPassword,editTextConfirmPassword,editTextName, editTextAddress, editTextBirthDate;
	Spinner spinnerSex;
	
	TextView textViewBirth;
	private TextView tv_head;
	private int year;
	private int month;
	private int day;
	
	Button btnCreateAccount;
	Context mContext = null;
	static final int DATE_DIALOG_ID = 999;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.register_title);
        
		//Must init the MedController here!!!
		if(MedController.mController == null) {
			MedController controller = new MedController(this);
		}
		mContext = this;

		//Set the UI handler
		MedController.setUIHandler(mUiHandler);

		// Get Refferences of Views
		editTextUserName=(EditText)findViewById(R.id.editTextUserName);
		editTextPassword=(EditText)findViewById(R.id.editTextPassword);
		editTextConfirmPassword=(EditText)findViewById(R.id.editTextConfirmPassword);
		editTextName=(EditText)findViewById(R.id.editTextName);
		editTextAddress=(EditText)findViewById(R.id.editTextAddress);
		textViewBirth=(TextView)findViewById(R.id.TextViewBirth);
		spinnerSex=(Spinner)findViewById(R.id.spinnerSex);
		
		TextView button_register = (TextView)findViewById(R.id.buttonCreateAccount);
		button_register.setClickable(true);
		button_register.setOnClickListener(this);

		setCurrentDateOnView();
		//datePickerBirth.setCalendarViewShown(false);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public void onClick(View v)
	{
		// TODO Auto-generated method stub

		String username=editTextUserName.getText().toString().trim();
		String password=editTextPassword.getText().toString().trim();
		String confirmPassword=editTextConfirmPassword.getText().toString().trim();
		String name=editTextName.getText().toString().trim();
		String address=editTextAddress.getText().toString().trim();
		byte role_type=1;
		@SuppressWarnings("deprecation")
		Date birthdate = new Date(year-1900, month, day);
		byte sex= (byte) spinnerSex.getLastVisiblePosition();

		Log.d(TAG, " username:"+username+" name:"+name+" address:"+address+" birthdate:"+birthdate+" sex:"+sex);

		// check if any of the fields are vaccant
		if(username.equals("")||password.equals("")||confirmPassword.equals(""))
		{
			Toast.makeText(getApplicationContext(), R.string.username_password_empty, Toast.LENGTH_LONG).show();
			return;
		}
		// check if both password matches
		if(!password.equals(confirmPassword))
		{
			Toast.makeText(getApplicationContext(), R.string.password_not_match, Toast.LENGTH_LONG).show();
			return;
		}
		else if(password.length()<6)
		{
			Toast.makeText(getApplicationContext(), R.string.password_minlength_6, Toast.LENGTH_LONG).show();
			return;
		}
		else
		{
			//do the register in background
			//only use two parameter for now, will add more parameter later. WTODO
			//function statement as register(String username,String password, String name, byte role_type, byte sex, byte age, String address)
			MedController.register(username, password,name, role_type,sex,birthdate,address);

			String processing = getResources().getString(R.string.processing);
			//show progress dialog
			pDialog = new ProgressDialog(MedRegisterActivity.this);
			pDialog.setMessage(processing);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(this, MedLoginActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

	//implement the UI handler
	Handler mUiHandler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case  MedController.REGISTER:
				//Medcontroller will sent this message back after it handled the request
				Log.d(TAG, "msg.arg1 = " + msg.arg1);
				if(msg.arg1 == 0) {
					try {
						JSONObject patient_info = (JSONObject) msg.obj;
						if (patient_info != null) {
							int patient_id = patient_info.getInt("pat_id");
							((MedPatientApp)getApplicationContext()).setPatientID(patient_id);
							Log.d(TAG, "patient id = " + patient_id);
							//Log.d(TAG, "patient name = " + patient_info.getString("name"));
						}
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						return;
					}

					// TODO: Need to launch doc list activity
					Intent intent = new Intent();
					intent.setClass(mContext, DocListActivity.class);
					intent.putExtra("registered", true);
					mContext.startActivity(intent);
					finish();
					Toast.makeText(MedRegisterActivity.this, "Register success", Toast.LENGTH_LONG).show();
				}
				else
					Toast.makeText(MedRegisterActivity.this, "Register failed!", Toast.LENGTH_LONG).show();
				pDialog.dismiss();
				break;  
			}
		}
	};

	// display current date
	public void setCurrentDateOnView() {
 
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
 
		// set current date into textview
		textViewBirth.setText(new StringBuilder()
			// Month is 0 based, just add 1
			.append(month + 1).append("-").append(day).append("-")
			.append(year).append(" "));
 	}
 
	public void onClickTVDateBirth (View v)
	{
		// TODO Auto-generated method stub
		showDialog(DATE_DIALOG_ID);
	}

 
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
		   // set date picker as current date
		   return new DatePickerDialog(this, datePickerListener,year, month,day);
		}
		return null;
	}
 
	private DatePickerDialog.OnDateSetListener datePickerListener 
                = new DatePickerDialog.OnDateSetListener() {
 
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
 
			// set current date into textview
			textViewBirth.setText(new StringBuilder()
				// Month is 0 based, just add 1
				.append(month + 1).append("-").append(day).append("-")
				.append(year).append(" "));
 
		}
	};

}

