package com.sixplus.med_sys_patient;

class QuesViewInfo {
	public int mType;
	public String mUrl;
	
	QuesViewInfo(int type, String url) {
		if (type <= MedConstants.IMAGE_TYPE) {
			mType = type;
		}
		
		if (url != null && !url.trim().equals("")) {
			mUrl = url;
		}
	}
}