package com.sixplus.med_sys_patient;

import java.util.*;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;
import android.content.res.Resources;
import android.content.Context;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import org.apache.http.util.EntityUtils;
import org.apache.http.HttpStatus;
import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import us.xdroid.util.ControllerBase;
import us.xdroid.util.ControllerBase.*;
import us.xdroid.util.xHelper;
import us.xdroid.util.HttpConnectionManager;
import us.xdroid.util.HttpConnectionManager.*;


public class MedController extends us.xdroid.util.ControllerBase{
    static final String TAG = "MedController";
    final static boolean DEBUG = MedConstants.DEBUG;

    public static final int LOGIN = 1000;
    public static final int DOC_LIST = 1001;
    public static final int DOC_DETAIL = 1002;
    public static final int REGISTER = 1003;
    public static final int ADD_DOCTOR = 1004;
    public static final int UPLOAD_FILE = 1020;
    public static final int ADD_QUESTION = 1021;
    public static final int QUESTION_LIST = 1022;
    public static final int GET_QUESTION = 1023;
    public static final int DOWNLOAD_FILE = 1024;
    
    /* add new action here */
    /*
    public static final int LOADMAINCATEGORY = 2000;
    
    
    public static final int GETAGENCIES = 3000;

    */
    static MedController mController = null;
    

    public MedController(Context context){
    	
    	HttpConnectionManager.SERVER_URL = MedConstants.SERVER_URL + MedConstants.PHP_API_URL;
//        HttpConnectionManager.SERVER_URL = "http://10.0.0.16/vtiger/modules/mobile/api.php";
        //HttpConnectionManager.SERVER_URL = "http://192.168.1.81:8811/modules/Mobile/api.php";
    	//HttpConnectionManager.SERVER_URL = "http://54.214.27.29/vtigercrm/modules/Mobile/api.php";
        init(context);
        mController = this;
    }

    @Override
    public void handleMessage(Message msg){
        switch(msg.what){
            case REGISTER:
        	    handleRegisterReq(msg);
        	break;
            case UPLOAD_FILE:
            	handleUploadFile(msg);
            	break;
            case DOWNLOAD_FILE:
                handleDownloadFile(msg);
                break;
            case LOGIN:
            case DOC_DETAIL:
            case ADD_DOCTOR:
            case ADD_QUESTION:
            case QUESTION_LIST:
            case GET_QUESTION:
            default:
                handleGenericReq(msg);
                break;
        }
	}

    static MyJsonResponse doPostJson(JSONObject obj){
        return HttpConnectionManager.doPostJson(obj);
    }

    private void handleUploadFile(Message msg) {
    	String filePath = (String) msg.obj;
    	if (DEBUG) Log.d(TAG, "handleUploadFile:" + filePath);
    	int fileType = msg.arg1;
    	String phpHandler = null;
    	MyJsonResponse resp = null;
    	
    	if (fileType < MedConstants.phpHandlerArray.length) {
    		phpHandler = MedConstants.phpHandlerArray[fileType];
    	} else {
    		Log.e(TAG, "upload fileType is not in defined range!");
    	}

    	if (phpHandler != null) {
    		String phpUrl = phpHandler;
    		resp = HttpConnectionManager.uploadFile(filePath, phpUrl);
    	}
         
    	if (resp != null) {
        	sendMessageToUI(msg.what,resp.ret,0,null);
    	}
    }
    
    private void handleDownloadFile(Message msg) {
    	int result = -1;
    	String downloadUrl = (String) msg.obj;
    	if (DEBUG) Log.d(TAG, "handleDownloadFile:" + downloadUrl);
    	String fileName = MedUtil.getFileName(downloadUrl);
    	int fileType = msg.arg1;
    	String filePath = null;
    	
    	if (fileType == MedConstants.AUDIO_TYPE) {
    		filePath = MedUtil.saveAudioDir;
    	} else if (fileType == MedConstants.IMAGE_TYPE) {
    		filePath = MedUtil.saveImageDir;
    	}

    	File file=new File(filePath + fileName);
    	if (file.exists()) {
    		if (DEBUG) Log.d(TAG, "file already existed!");
    		result = 0;
    	} else {
        	if (downloadUrl != null) {
        		try {
        		    HttpConnectionManager.downloadFile(downloadUrl, filePath, fileName);
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
        	}
        	if (file.exists()) {
        		if (DEBUG) Log.d(TAG, "file download success!");
        		result = 0;
        	}
    	}
    	
    	sendMessageToUI(msg.what,result,msg.arg2,null);
    }
    
    void handleRegisterReq(Message msg){
        MyJsonResponse resp = doPostJson((JSONObject)msg.obj);

            JSONObject jso = resp.obj;
            if(jso!=null){
                //CampusModel.parseLogin(jso);
            	Log.d("Register", jso.toString());
            	try{
            		if(jso.getBoolean("success")) {
            			Log.d("Register", "success");
            			//result array with the returned rows 
            			JSONObject result = jso.getJSONObject("result");
            			xHelper.log("Register Successfully!!");
            			sendMessageToUI(REGISTER,0,0,result);
            		}
            		else {
            			Log.d("Register", "fail");
            			xHelper.log("Register Failed!!");
            			sendMessageToUI(REGISTER,-1,0,null);
            		}
                    xHelper.log(jso.toString());
            	} catch (Exception e) {
            		e.printStackTrace();
            		sendMessageToUI(REGISTER,-1,0,null);
            	}
                return;
            }
		
         sendMessageToUI(REGISTER,-1,0,null);
    }
    
    void handleGenericReq(Message msg){
        MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
        
        JSONObject jso = resp.obj;
        if(jso!=null){
            if (DEBUG) Log.d(TAG, jso.toString());
            sendMessageToUI(msg.what,0,0,jso);
            return;
        }
        sendMessageToUI(msg.what,-1,0,null);
    }
    
    //Register new account
    public static void register(String username,String password, String name, byte role_type, byte sex, Date birthdate, String address){
    //public static void register(String username,String password){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("pat"));
			obj.put("sub", new String("register"));
			obj.put("login_id", username);
			obj.put("login_password", password);
			obj.put("name", name);
			obj.put("role_type", role_type);
			obj.put("sex", sex);
			obj.put("birthdate", birthdate);
			obj.put("address", address);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(REGISTER,obj);
    }
    
    public static void findDocList(int patient_id){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("rel"));
			obj.put("sub", "query");
			obj.put("pat_id", patient_id);
			obj.put("doc_id", 0);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(DOC_LIST,obj);
    }

    public static void addDoctor(int patID, int docAuth) {
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("rel"));
			obj.put("sub", "addRelation");
			obj.put("pat_id", patID);
			obj.put("doc_auth", docAuth);
			obj.put("status", 1);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(ADD_DOCTOR,obj);
    }
    
    public static void docDetail(int docID) {
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("doc"));
			obj.put("sub", "query");
			obj.put("doc_id", docID);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(DOC_DETAIL,obj);
    }

    public static void findQuestionList(int patient_id, int doc_id){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", "queryList");
			obj.put("pat_id", patient_id);
			obj.put("doc_id", doc_id);
			obj.put("seq_id", 0);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(QUESTION_LIST,obj);
    }

    public static void getQuestion(int questionID, int patID){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", "query");
			obj.put("question_id", questionID);
			obj.put("pat_id", patID);
			obj.put("doc_id", 0);
			obj.put("seq_id", 0);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(GET_QUESTION,obj);
    }


    public static void uploadFile(String filePath, int fileType){
    	if (DEBUG) Log.d(TAG, "uploadFile: " + filePath);
    	mController.sendRequest(UPLOAD_FILE, fileType, filePath);
    }
    
    public static void downloadFile(String filePath, int fileType, int messageID){
    	if (DEBUG) Log.d(TAG, "downloadFile: " + filePath);
    	mController.sendRequest(DOWNLOAD_FILE, fileType, messageID, filePath);
    }
    
    //keep login code in case we will use it later
    public static void inserQuestion(PatientQuestion question, int pat_id, int doc_id){
        JSONObject obj = new JSONObject();
        boolean handled = false;
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", "addMessage");
			obj.put("direction", question.mQuestionDirection);
			obj.put("question_id", question.mQuestionID);
			obj.put("pat_id", pat_id);
			obj.put("doc_id", doc_id);
			obj.put("index", question.mIndex);
			
			if (question.mQuestionPictureUrl != null) {
				obj.put("attach_type", MedConstants.IMAGE_TYPE);
				File file = new File(question.mQuestionPictureUrl);
				if (file != null) {
					obj.put("attach_url", MedConstants.SERVER_URL + MedConstants.DOWNLOAD_IMAGE_URL + file.getName());
				}
				handled = true;
			}

			if (question.mQuestionWavUrl != null && !handled) {
				obj.put("attach_type", MedConstants.AUDIO_TYPE);
				File file = new File(question.mQuestionWavUrl);
				if (file != null) {
					obj.put("attach_url", MedConstants.SERVER_URL + MedConstants.DOWNLOAD_AUDIO_URL + file.getName());
				}
				handled = true;
			}

	        if (question.mQuestionText != null && !handled) {
	        	obj.put("message", question.mQuestionText);
	        	handled = true;
	        }
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        
        if (handled) {
        	if (DEBUG) Log.d(TAG, "add question request!");
            mController.sendRequest(ADD_QUESTION,obj);
        }
    }

    //keep login code in case we will use it later
    public static void login(String username,String password){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("pat"));
			obj.put("sub", "login");
			obj.put("login_id", username);
			obj.put("login_password", password);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(LOGIN,obj);
    }
}