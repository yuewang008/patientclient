package com.sixplus.med_sys_patient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sixplus.med_sys_patient.R;

import us.xdroid.util.xHelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DocListActivity extends Activity implements OnItemClickListener {
	private static final String TAG = "DocListActivity";
	final static boolean DEBUG = MedConstants.DEBUG;

	final static long BACK_PRESSED_DELAY = 5000;

    // Progress Dialog
    private ProgressDialog pDialog;
    private int mPatID = 0;
    private List<doctorInfo> docList = null;
    private ListView mDocListView = null;
    private TextView mFindNoDocTextView = null;
    private boolean mBackPressed = false;
    private boolean mRegistered = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.med_doc_list);
        
        mDocListView = (ListView) findViewById(R.id.doctorlistView);
        mDocListView.setOnItemClickListener(this);
        
        mFindNoDocTextView = (TextView)findViewById(R.id.find_no_doctor);
        
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.doctor_list);
        
        mPatID = ((MedPatientApp)getApplicationContext()).getPatientID();

        //Must init the MedController here!!!
  	  	if(MedController.mController == null) {
  	  		MedController controller = new MedController(this);
  	  	}

		TextView tx=(TextView)findViewById(R.id.add_doctors);
        tx.setClickable(true);
        tx.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(DocListActivity.this, MedAddDoctorActivity.class);
				startActivity(intent);
			}
		});
        
        mRegistered = getIntent().getBooleanExtra("registered", false);
        
        pDialog = new ProgressDialog(this);
        
        if (!mRegistered) {
            mFindNoDocTextView.setVisibility(View.GONE);
    		String processing = getResources().getString(R.string.processing);
    		//show progress dialog
    		pDialog.setMessage(processing);
    		pDialog.setIndeterminate(false);
    		pDialog.setCancelable(true);
    		pDialog.show();
        }
    }

    @Override
    protected void onResume() {
    	super.onResume();
    	mPatID = ((MedPatientApp)getApplicationContext()).getPatientID();

  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);
  	  	if (!mRegistered) {
  	    	//do the login in background, for registered user, do not query for doctors.
  	    	MedController.findDocList(mPatID);
  	  	} else {
  	  	    mRegistered = false;
  	  	}
    }
    
    private Handler mHandler = new Handler();
    
    private Runnable mBackRunnable = new Runnable() {
        public void run () {
            mBackPressed = false;
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	if (DEBUG) Log.d(TAG, "pressed!");
        	if (mBackPressed) {
        		finish();
        	} else {
            	Toast.makeText(DocListActivity.this, R.string.press_back_twice_exit, Toast.LENGTH_LONG).show();
            	mBackPressed = true;
            	mHandler.postDelayed(mBackRunnable, BACK_PRESSED_DELAY);
        	}
        }
        return true;
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    	Log.d(TAG, "position = " + position + " id = " + id);
    	doctorInfo docInfo = docList.get(position);
		Intent intent = new Intent();
		intent.setClass(this, DocDetailActivity.class);
		intent.putExtra("doc_id", docInfo.mDoc_id);
		intent.putExtra("name", docInfo.mName);
		intent.putExtra("sex", docInfo.mSex);
		intent.putExtra("role", docInfo.mRole);
		intent.putExtra("address", docInfo.mAddress);
		intent.putExtra("resume", docInfo.mResume);
		((MedPatientApp)getApplicationContext()).setDocID(docInfo.mDoc_id);
		startActivity(intent);
    }

    private List<Map<String, Object>> getData(JSONArray array) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        docList = new ArrayList<doctorInfo>();
        String[] roleArray = getResources().getStringArray(R.array.doc_roles);
 
        int length = array.length();
        for(int i=0;i<length;i++){
        	try {
                JSONObject object = array.getJSONObject(i);
                Map<String, Object> map = new HashMap<String, Object>();
                int doc_id = object.getInt("doc_id");
                String name = object.getString("name");
                int sex = object.getInt("sex");
                String role = roleArray[object.getInt("role_type")];
                String address = object.getString("address");
                map.put("doc_id", doc_id);
                map.put("name", name);
                map.put("sex", sex);
                map.put("role", role);
				map.put("address", address);
				doctorInfo doctorInfoInstance = new doctorInfo(doc_id, name, sex, role, address, null);
				docList.add(doctorInfoInstance);
	            list.add(map);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return list;
    }
    
    //implement the UI handler
	Handler mUiHandler = new Handler() {
      @Override
      public void handleMessage(android.os.Message msg) {
          switch (msg.what) {
			case  MedController.DOC_LIST:
                //Medcontroller will sent this message back after it handled the request
				Log.d(TAG, "msg.arg1 = " + msg.arg1);
				if(msg.arg1 == 0) {
					if (msg.obj != null) {
						try{
							JSONObject jso = (JSONObject)msg.obj;
		            		if(jso.getBoolean("success")) {
		            			if(DEBUG) Log.d(TAG, "find doclist success!");
		            			//result array with the returned rows 
		            			JSONArray result = jso.getJSONArray("result");
								SimpleAdapter adapter = new SimpleAdapter(DocListActivity.this,getData(result),R.layout.med_doctor_list_view,
						                new String[]{"name","role","address"},
						                new int[]{R.id.name,R.id.title,R.id.address});
								mDocListView.setAdapter(adapter);
								adapter.notifyDataSetChanged();
								mDocListView.smoothScrollToPosition(docList.size());
								mFindNoDocTextView.setVisibility(View.GONE);
		            		}
		            		else {
		            			mFindNoDocTextView.setVisibility(View.VISIBLE);
		            		}
		                    xHelper.log(jso.toString());
		            	} catch (Exception e) {
		            		e.printStackTrace();
		            	}
					}
				} else {
					Toast.makeText(DocListActivity.this, R.string.find_doc_list_failed, Toast.LENGTH_LONG).show();
				}
				
				pDialog.dismiss();
              break;
		  }
	   }
	};
	
	class doctorInfo {
		public int mDoc_id;
		public String mName;
		public int mSex;
		public String mRole;
		public String mAddress;
		public String mResume;
		public doctorInfo(int doc_id, String name, int sex, String role, String address, String resume) {
			mDoc_id = doc_id;
			mName = name;
			mSex = sex;
			mRole = role;
			mAddress = address;
			mResume = resume;
		}
	}
}
