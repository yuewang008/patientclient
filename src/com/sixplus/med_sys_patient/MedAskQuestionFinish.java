package com.sixplus.med_sys_patient;

import com.sixplus.med_sys_patient.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MedAskQuestionFinish extends Activity implements OnClickListener{
	private static final String TAG = "MedAskQuestionFinish";
	private static boolean DEBUG = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.med_ask_doc_finish_view);
		
		TextView tilte = (TextView)findViewById(R.id.navigation_title);
		TextView content = (TextView)findViewById(R.id.ask_question_result);

		boolean success = getIntent().getBooleanExtra("success", false);
		if (success) {
			tilte.setText(R.string.title_activity_med_ask_question_success);
	        content.setText(R.string.content_activity_med_ask_question_success);
		} else {
			tilte.setText(R.string.title_activity_med_ask_question_failed);
			content.setText(R.string.content_activity_med_ask_question_failed);
		}

		TextView tx=(TextView)findViewById(R.id.return_question_list);
        tx.setClickable(true);
        tx.setOnClickListener(this);
	}

	public void onClick(View v) {
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login_result, menu);
		return true;
	}
}
