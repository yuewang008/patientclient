package com.sixplus.med_sys_patient;

import java.util.ArrayList;

import 	android.app.Application;

public class MedPatientApp extends Application {
    private int pat_id;
    private int doc_id;
    private ArrayList<PatientQuestion> mPatQuestionList;
    
    public ArrayList<PatientQuestion> getPatQuestionList() {
    	return mPatQuestionList;
    }
    
    public void setPatQuestionList(ArrayList<PatientQuestion> patQuestionList) {
    	if (patQuestionList != null) {
    		mPatQuestionList = patQuestionList;
    	}
    }
    
    public int getPatientID () {
    	return pat_id;
    }
    
    public void setPatientID (int id) {
    	if (id <= 0) return;
        pat_id = id;
    }
    
    public int getDocID () {
    	return doc_id;
    }
    
    public void setDocID (int id) {
    	if (id <= 0) return;
        doc_id = id;
    }
}
