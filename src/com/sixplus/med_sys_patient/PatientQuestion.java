package com.sixplus.med_sys_patient;

public class PatientQuestion{
	public PatientQuestion() {
		mQuestionText = "";
		mQuestionPictureUrl = null;
		mQuestionPicturePreviewUrl = null;
		mQuestionWavUrl = null;
		mQuestionDirection = 0;
		mQuestionID = 0;
		mMessageID = 0;
		mIndex = 0;
	}
	public String mQuestionText;
	public String mQuestionPictureUrl;
	public String mQuestionPicturePreviewUrl;
	public String mQuestionWavUrl;
	public int mQuestionDirection;
	public int mQuestionID;
	public int mMessageID;
	public int mIndex;
}