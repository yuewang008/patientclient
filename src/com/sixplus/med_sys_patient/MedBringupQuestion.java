package com.sixplus.med_sys_patient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import us.xdroid.util.xHelper;

import com.sixplus.med_sys_patient.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MedBringupQuestion extends Activity 
    implements View.OnClickListener, 
               PatientQuestionListFragment.ViewClickListener,
               TextWatcher {
    static final String TAG = "MedBringupQuestion";
    final static boolean DEBUG = MedConstants.DEBUG;
    final int[] mImageTypes = {R.id.image_button1, R.id.image_button2, R.id.image_button3, R.id.image_button4, R.id.image_button5, R.id.image_button6};

    private static final int CAMERA_PIC_REQUEST = 2500;
    private static final int SELECT_PICTURE = 2501;
    private static final int MAX_RECORDING_TIME = 60;
    private static final int MAX_IMAGE_HEIGHTH = 900;

    private File mPhotoFile = null;
    private ArrayList<PatientQuestion> mPatQuestionList = new ArrayList<PatientQuestion>();
    private EditText mQuestionText;
    private PatientQuestionListFragment mlistfragment;
    private MedAddImageDialog addImageDialog;
    private MedAddAudioDialog addAudioDialog;
    private ImageButton mFinishButton;
    private ImageButton addImageView;
    private ImageButton addAudioView;
    private Button submitButton;
    private ProgressDialog pDialog;

    private final static int MAX_ICON_HEIGHT = 100;
    private final static int MAX_ICON_WIDTH = 50;

    private MediaRecorder mMediaRecorder;
    private int mUploadFileNum = 0;
    private int mDownloadFileNum = 0;
    private int mFailedFiles = 0, mSuccessFiles = 0;
    private int mQuestionItemNum = 0;
    private int mFailedItems = 0, mSuccessItems = 0;
    private int mQuesID = 0;
	private boolean mAskFinish = false;
	private int mPatID;
	private int mDocID;
	
	private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DEBUG) Log.d(TAG, "onCreate");
        setContentView(R.layout.med_ask_doc_main_view);

        mQuestionText = (EditText)findViewById(R.id.edit_text_question);
        mQuestionText.addTextChangedListener(this);

        mFinishButton = (ImageButton)findViewById(R.id.add_text);
        mFinishButton.setClickable(true);
        mFinishButton.setOnClickListener(this);
		mFinishButton.setEnabled(false);
		mFinishButton.setAlpha(0.5f);

        addImageView = (ImageButton)findViewById(R.id.add_image);
        addImageView.setClickable(true);
        addImageView.setFocusable(true);

        addImageView.setOnClickListener(this);

        addAudioView = (ImageButton)findViewById(R.id.add_audio);
        addAudioView.setClickable(true);
        addAudioView.setOnClickListener(this);

        submitButton = (Button) findViewById(R.id.navigation_right_button);
        submitButton.setText(R.string.submit_content);
        submitButton.setVisibility(View.INVISIBLE);
        submitButton.setOnClickListener(this);

        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.question_title);

        mlistfragment = (PatientQuestionListFragment)getFragmentManager().findFragmentById(R.id.patient_question_list_fragment);
        mlistfragment.setList(mPatQuestionList);
        mlistfragment.setOnViewClickListener(this);

        MedUtil.initialFolders();
        
        if(MedController.mController == null) {
            new MedController(this);
        }
        MedController.setUIHandler(mUiHandler);
        
        db = openOrCreateDatabase(MedConstants.DBNAME, Context.MODE_PRIVATE, null);

        boolean recreated = false;
        if (savedInstanceState != null) {
        	recreated = savedInstanceState.getBoolean("recreated", false);
        	if (DEBUG) Log.d(TAG, "recreated = " + recreated);
        }

        if (recreated) {
        	if (DEBUG) Log.d(TAG, "recreated");
        	mQuesID = savedInstanceState.getInt("question_id");
        	mPatID = savedInstanceState.getInt("pat_id");
        	mDocID = savedInstanceState.getInt("doc_id");
        	((MedPatientApp)getApplicationContext()).setPatientID(mPatID);
        	((MedPatientApp)getApplicationContext()).setDocID(mDocID);
        	recoverQuestion();
            mlistfragment.update();
        } else {
            iniDatabase();
        	mPatID = ((MedPatientApp)getApplicationContext()).getPatientID();
        	mDocID = ((MedPatientApp)getApplicationContext()).getDocID();
            // if the question id extra is not empty, load the question and display it.
            mQuesID = getIntent().getIntExtra("question_id", 0);
            if (mQuesID > 0) {
              	MedController.getQuestion(mQuesID, mPatID);
        		String processing = getResources().getString(R.string.processing);
        		//show progress dialog
        		pDialog = new ProgressDialog(this);
        		pDialog.setMessage(processing);
        		pDialog.setIndeterminate(false);
        		pDialog.setCancelable(true);
        		pDialog.show();
            }
        }
        
        
    }

    @Override
    protected void onSaveInstanceState (Bundle outState) {
    	if (DEBUG) Log.d(TAG, "onSaveInstanceState");
    	outState.putBoolean("recreated", true);
    	outState.putInt("question_id", mQuesID);
    	outState.putInt("pat_id", mPatID);
    	outState.putInt("doc_id", mDocID);
    }

    @Override
    protected void onPause() {
    	if (DEBUG) Log.d(TAG, "onPause");
    	super.onPause();
    }

    @Override
    protected void onResume() {
    	if (DEBUG) Log.d(TAG, "onResume");
        super.onResume();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	if (DEBUG) Log.d(TAG, "onDestroy");
    	mQuestionText.removeTextChangedListener(this);
    	db.close();
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
        case R.id.add_text:
            String questionText = mQuestionText.getText().toString().trim();
            if(!questionText.equals("")) {
                addQuestionItem(null, null, null, questionText);
                mQuestionText.setText("");
                addAudioView.requestFocus();
            } else {
                addAudioView.requestFocus();
            }
            break;
        case R.id.add_audio:
            addAudioDialog = new MedAddAudioDialog(MedBringupQuestion.this,
                    R.style.SelectScreenDialog);
            addAudioDialog.show();
            break;
        case R.id.add_image:
            addImageDialog = new MedAddImageDialog(MedBringupQuestion.this,
                    R.style.SelectScreenDialog);
            addImageDialog.show();
            break;
        case R.id.navigation_right_button:
            submitQuestion();
            submitButton.setClickable(false);
    		String processing = getResources().getString(R.string.processing);
    		//show progress dialog
    		pDialog = new ProgressDialog(this);
    		pDialog.setMessage(processing);
    		pDialog.setIndeterminate(false);
    		pDialog.setCancelable(true);
    		pDialog.show();
            break;
        default:
        }
    }

    private void iniDatabase() {
    	dropQuestion();
	    db.execSQL("CREATE TABLE IF NOT EXISTS current_question (question_id INTEGER, message_id INTEGER, Text varchar(100) default NULL, PictureUrl varchar(200) default NULL, PicturePreviewUrl varchar(200) default NULL, WavUrl varchar(200) default NULL, Direction SMALLINT, qesindex SMALLINT)");
    }

    private void dropQuestion() {
    	db.execSQL("DROP TABLE IF EXISTS current_question");
    }

    private void recoverQuestion() {
    	Cursor cr = null;
		String sql = "SELECT * FROM current_question";
		cr = db.rawQuery(sql, null);
        while(cr.moveToNext()){
        	PatientQuestion patQues = new PatientQuestion();
        	patQues.mIndex = cr.getInt(cr.getColumnIndex("qesindex"));
        	patQues.mMessageID = cr.getInt(cr.getColumnIndex("message_id"));
        	patQues.mQuestionDirection = cr.getInt(cr.getColumnIndex("Direction"));
        	patQues.mQuestionID = cr.getInt(cr.getColumnIndex("question_id"));
        	patQues.mQuestionPicturePreviewUrl = cr.getString(cr.getColumnIndex("PicturePreviewUrl"));
        	patQues.mQuestionPictureUrl = cr.getString(cr.getColumnIndex("PictureUrl"));
        	patQues.mQuestionText = cr.getString(cr.getColumnIndex("Text"));
        	patQues.mQuestionWavUrl = cr.getString(cr.getColumnIndex("WavUrl"));
        	mPatQuestionList.add(patQues.mIndex, patQues);
        }
        cr.close();
    }

    private void addItemtoDB(PatientQuestion patQues) {
    	Cursor cr = null;
		String sql = "INSERT INTO current_question (question_id, message_id, Text, PictureUrl, PicturePreviewUrl, WavUrl, Direction, qesindex) VALUES(?,?,?,?,?,?,?,?)";
		cr = db.rawQuery(sql, new String[]{String.valueOf(patQues.mQuestionID),
				String.valueOf(patQues.mMessageID),
				patQues.mQuestionText==null?"":patQues.mQuestionText,
				patQues.mQuestionPictureUrl==null?"":patQues.mQuestionPictureUrl,
				patQues.mQuestionPicturePreviewUrl==null?"":patQues.mQuestionPicturePreviewUrl,
				patQues.mQuestionWavUrl==null?"":patQues.mQuestionWavUrl,
				String.valueOf(patQues.mQuestionDirection),
				String.valueOf(patQues.mIndex)});
        if (cr.getCount() <= 0) Log.e(TAG, "insert message info sqlite failed!");
    }

    class MedAddImageDialog extends Dialog {

        int mImageType = 0;
        Context context;
        public MedAddImageDialog(Context context, int theme){
            super(context, theme);
            this.context = context;
        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.setContentView(R.layout.add_image_dialog_view);
            for (int i = 0; i < 6; i++) {
                ImageView imageview = (ImageView)findViewById(mImageTypes[i]);
                imageview.setOnClickListener(new AddImageDialogOnClickListener());
            }
        }

        class AddImageDialogOnClickListener implements android.view.View.OnClickListener {
            AlertDialog selectPictureSoruce;
            public void onClick(View arg0) {
                // find the image type
                for (int i = 0; i < mImageTypes.length; i++) {
                    if (arg0.getId() == mImageTypes[i]) {
                        mImageType = i;
                        break;
                    }
                }
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.med_select_image_or_camera_dialog,
                     (ViewGroup) findViewById(R.id.med_select_image_or_camera_dialog));

                selectPictureSoruce = new AlertDialog.Builder(MedBringupQuestion.this).setTitle(R.string.select_image_type).setView(layout).show();

                Button selectButton = (Button)layout.findViewById(R.id.select_image);
                selectButton.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Log.d(TAG, "onClick select image");
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
                        selectPictureSoruce.dismiss();
                        addImageDialog.dismiss();
                    }
                });

                Button cameraButton = (Button)layout.findViewById(R.id.take_picture);
                cameraButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Log.d(TAG, "onClick camera");
                        String state = Environment.getExternalStorageState();
                        if (state.equals(Environment.MEDIA_MOUNTED)) {
                            String fileName = ((MedPatientApp)getApplicationContext()).getPatientID() + "_" + System.currentTimeMillis() + ".jpg";
                            Log.d(TAG, "photo filename is " + fileName + " MedUtil.saveImageDir = " + MedUtil.saveImageDir);
                            mPhotoFile = new File(MedUtil.saveImageDir, fileName);
                            mPhotoFile.delete();
                            if (!mPhotoFile.exists()) {
                                try {
                                    mPhotoFile.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Toast.makeText(MedBringupQuestion.this,
                                            "photo file is null",
                                            Toast.LENGTH_LONG).show();
                                    return;
                                }
                            }
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPhotoFile));
                            startActivityForResult(intent, CAMERA_PIC_REQUEST);
                        } else {
                            Toast.makeText(MedBringupQuestion.this,
                                    "no sdcard available", Toast.LENGTH_LONG)
                                    .show();
                        }
                        selectPictureSoruce.dismiss();
                        addImageDialog.dismiss();
                    }
                });
            }
        }

        class SelectOrCameraDialog extends Dialog {
            Context context;
            public SelectOrCameraDialog(Context context, int theme){
                super(context, theme);
                this.context = context;
            }
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                this.setContentView(R.layout.med_select_image_or_camera_dialog);

            }

            class SelectImageDialogOnClickListener implements android.view.View.OnClickListener {
                public void onClick(View arg0) {
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        Bitmap photo = null;
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (mPhotoFile != null && mPhotoFile.exists()) {
                if (DEBUG) Log.d(TAG, "mPhotoFile.getPath() " + mPhotoFile.getPath());
                String photoPath = photoCompass(mPhotoFile.getPath(), MAX_IMAGE_HEIGHTH, MedConstants.ORIGINAL);
                String previewPhotoPath = photoCompass(mPhotoFile.getPath(), MAX_ICON_HEIGHT, MedConstants.PREVIEW);
                addQuestionItem(photoPath, previewPhotoPath, null, null);
                mPhotoFile.delete();
                mPhotoFile = null;
            } else {
                Toast.makeText(MedBringupQuestion.this, "photo_file_isnull",
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == SELECT_PICTURE) {
            Uri selectedImageUri = data.getData();
            String selectedImagePath = getPath(selectedImageUri);
            Log.d(TAG, "Image Path : " + selectedImagePath);
            String photoPath = photoCompass(selectedImagePath, MAX_IMAGE_HEIGHTH, MedConstants.ORIGINAL);
            String previewPhotoPath = photoCompass(selectedImagePath, MAX_ICON_HEIGHT, MedConstants.PREVIEW);

            addQuestionItem(photoPath, previewPhotoPath, null, null);
        }
    }

    private String photoCompass(String originalPhoto, int limitLength, int type) {
        String photoPath = null;
        int max_length = 0;
        int min_length = 0;
        int inSampleSize = 1;
        float scale = getResources().getDisplayMetrics().density;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 2;
        BitmapFactory.decodeFile(originalPhoto, options);
        int imageHeight = Math.round(options.outHeight * scale);
        int imageWidth = Math.round(options.outWidth * scale);
        String imageType = options.outMimeType;
        if (DEBUG) Log.d(TAG, "inScreenDensity = " + options.inScreenDensity + " inDensity = " + options.inDensity);
        if (DEBUG) Log.d(TAG, "the original bitmap size is : " + imageHeight + "X" + imageWidth);

        if (imageHeight >= imageWidth) {
        	max_length = imageHeight;
        	min_length = imageWidth;
        } else {
        	max_length = imageWidth;
        	min_length = imageHeight;
        }

        if (max_length > limitLength) {
        	inSampleSize = Math.round((float) max_length / (float) limitLength);
        	min_length = Math.round(min_length*(float) limitLength / (float) max_length);
        	max_length = limitLength;
        }
        if (DEBUG) Log.d(TAG, "inSampleSize:" + inSampleSize);

        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;

        Bitmap photo = BitmapFactory.decodeFile(originalPhoto, options);
        try {
            String photoFileName = new File(originalPhoto).getName();
          	photoFileName = photoFileName.substring(0, photoFileName.length()-4);
            if (type == MedConstants.PREVIEW) {
            	photoFileName = photoFileName + "_prev";
            } else if (type == MedConstants.ORIGINAL) {
            	photoFileName = photoFileName + "_ori";
            }
            photoPath = saveImageAsJpeg(photoFileName, photo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photoPath;
    }
    
    // save photo to file and return the file path.
    public String saveImageAsJpeg(String picName, Bitmap bitmap) throws IOException {
        File f = new File(MedUtil.saveImageDir + picName + ".jpg");
        if (f.exists()) {
        	return f.getPath();
        }

        if (DEBUG) Log.d(TAG, "file path = " + f.getPath());
        f.createNewFile();
        FileOutputStream fOut = null;
        try {
             fOut = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
             e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, fOut);
        try {
                fOut.flush();
        } catch (IOException e) {
                e.printStackTrace();
        }
        try {
                fOut.close();
        } catch (IOException e) {
                e.printStackTrace();
        }
        String path  = f.getPath();
        if (DEBUG) Log.d(TAG, "saveImageAsJpeg path = " + path);
        return path;
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public class MedAddAudioDialog extends Dialog implements View.OnClickListener {
        int mImageType = 0;
        Context context;
        private boolean mStartRecording = false;
        private Button startButton;
        private Button cancelButton;
        private TextView stateHint;
        private Timer timer = new Timer();
        private int recordingTime = 0;
        private String mAudioFileName = null;
        boolean mRecordStarted = false;

        public MedAddAudioDialog(Context context, int theme){
            super(context, theme);
            this.context = context;
        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.setContentView(R.layout.med_record_view);

            startButton = (Button) findViewById(R.id.start_record);
            startButton.setOnClickListener(this);
            cancelButton = (Button) findViewById(R.id.cancel_record);
            cancelButton.setOnClickListener(this);
            stateHint = (TextView) findViewById(R.id.state_hint);
            initializeAudioRecording();
        }

        @Override
        public void onClick(View arg0) {
            switch(arg0.getId()) {
            case R.id.start_record:
                mStartRecording = !mStartRecording;
                if (mStartRecording) {
                    timer.schedule(task, 1000, 1000);
                    startRecording();
                    stateHint.setText(R.string.recording);
                    startButton.setText(R.string.record_finish);
                } else {
                    timer.cancel();
                    stopRecording();
                    addQuestionItem(null, null, mAudioFileName, null);
                    addAudioDialog.dismiss();
                }
                break;
            case R.id.cancel_record:
                addAudioDialog.dismiss();
                break;
            default:
            }
        }

        private void stopRecording() {
            mMediaRecorder.stop();
            mMediaRecorder.release();
            mRecordStarted = false;
        }

        private void startRecording() {
            try {
                mMediaRecorder.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mMediaRecorder.start();
            mRecordStarted = true;
        }

        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                switch (msg.what) {
                case 1:
                    recordingTime++;
                    stateHint.setText(getString(R.string.recording) + (60-recordingTime));
                    if(recordingTime >= MAX_RECORDING_TIME){
                        timer.cancel();
                        stopRecording();
                        addQuestionItem(null, null, mAudioFileName, null);
                        addAudioDialog.dismiss();
                    }
                }
            }
        };

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 1;
                handler.sendMessage(message);
            }
        };

        private void initializeAudioRecording() {
            mMediaRecorder = new MediaRecorder();
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mAudioFileName = MedUtil.saveAudioDir + mPatID + "_" + System.currentTimeMillis() + ".3gp";
            mMediaRecorder.setOutputFile(mAudioFileName);
        }

        class StartRecordOnClickListener implements android.view.View.OnClickListener {
            public void onClick(View arg0) {
            }
        }

        @Override
        public void onStop() {
        	super.onStop();
        	if (mRecordStarted) {
        		stopRecording();
        	}
        }
    }

    @Override
    public void onViewClick(View view) {
        QuesViewInfo quesInfo = (QuesViewInfo)view.getTag();
        if (quesInfo != null) {
        	String filePath = MedUtil.getFileName(quesInfo.mUrl);
    		if (quesInfo.mType == MedConstants.AUDIO_TYPE) {
    			filePath = MedUtil.saveAudioDir + filePath;
    		} else if (quesInfo.mType == MedConstants.IMAGE_TYPE) {
    			filePath = MedUtil.saveImageDir + filePath;
    		}
        	Log.d(TAG, "quesInfo:" + quesInfo.mType + " / " + quesInfo.mUrl);
        	if (quesInfo.mType == MedConstants.IMAGE_TYPE) {
        		Intent intent = new Intent();
        		intent.setClass(this, MedViewPhotoActivity.class);
        		intent.putExtra("imagePath", filePath);
        		startActivity(intent);
        	} else if (quesInfo.mType == MedConstants.AUDIO_TYPE) {
        		Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(filePath), "video/amr");
                startActivity(intent);
        	}
        }
    }

    private void addQuestionItem(String imagePath, String imagePreviewPath, String audioPath, String text) {
    	addQuestionItem(imagePath, imagePreviewPath, audioPath, text, mQuesID, 0, 0);
    }
    
    private void addQuestionItem(String imagePath, String imagePreviewPath, String audioPath, String text, int questionID, int messageID, int direct) {
        PatientQuestion patQues = new PatientQuestion();
        if (text != null) {
            patQues.mQuestionText = text.equals("") ? "" : text;
        }
        if (imagePath != null && !imagePath.equals("")) {
            patQues.mQuestionPictureUrl = imagePath;
        }
        if (imagePreviewPath != null && !imagePreviewPath.equals("")) {
            patQues.mQuestionPicturePreviewUrl = imagePreviewPath;
        }
        if (audioPath != null && !audioPath.equals("")) {
            patQues.mQuestionWavUrl = audioPath;
        }
        patQues.mQuestionDirection = direct;
        patQues.mQuestionID = questionID;
        patQues.mMessageID = messageID;
        mPatQuestionList.add(patQues);
        patQues.mIndex = mPatQuestionList.indexOf(patQues);
        mlistfragment.update();
        if (mPatQuestionList.size() > 0 && messageID == 0) {
            submitButton.setVisibility(View.VISIBLE);
            submitButton.setClickable(true);
        }
        addItemtoDB(patQues);
    }

    private void submitQuestion() {
        if (mPatQuestionList.size() < 1) {
            return;
        }
        Log.d(TAG, "submitQuestion()");

        for (PatientQuestion item : mPatQuestionList) {
        	// if mQuestionID is not 0, this item should have already been added.
        	Log.d(TAG, "submitQuestion " + item.mMessageID);
        	if (item.mMessageID != 0) {
        		continue;
        	}

            if(item.mQuestionPictureUrl != null) {
            	if (DEBUG) Log.d(TAG, "upload picture: " + item.mQuestionPictureUrl);
            	mUploadFileNum++;
                uploadQuesImage(item.mQuestionPictureUrl);
            }

            if(item.mQuestionWavUrl != null) {
            	if (DEBUG) Log.d(TAG, "upload audio: " + item.mQuestionWavUrl);
            	mUploadFileNum++;
                uploadQuesAudio(item.mQuestionWavUrl);
            }
        }

        insertQuestion(mPatQuestionList);
    }

    private boolean insertQuestion(ArrayList<PatientQuestion> questionList) {
    	PatientQuestion item;
    	mQuestionItemNum = 0;
    	mSuccessItems = 0;
    	mFailedItems = 0;
    	int pat_id = ((MedPatientApp)getApplicationContext()).getPatientID();
    	int doc_id = ((MedPatientApp)getApplicationContext()).getDocID();
    	if (mQuesID == 0) {
    		// current question has not been added into database.
    		// add the first item to database to get question ID.
    		item = mPatQuestionList.get(0);
    		MedController.inserQuestion(item, pat_id, doc_id);
    		mQuestionItemNum = 1;
    	} else {
    		// current question has been added into database. But there may be some new items need to be added.
            for (PatientQuestion mesg : mPatQuestionList) {
            	if (mesg.mMessageID == 0) {
            		// item.mMessageID == 0 means the question item has not benn added into server.
                	MedController.inserQuestion(mesg, pat_id, doc_id);
                	mQuestionItemNum++;
            	}
            }
    	}
        return true;
    }

    private void uploadQuesImage(String imagePath) {
        MedController.uploadFile(imagePath, MedConstants.IMAGE_TYPE);
    }

    private void uploadQuesAudio(String audioPath) {
        MedController.uploadFile(audioPath, MedConstants.AUDIO_TYPE);
    }

    private void downloadQuesImage(String imagePath, int messageID) {
        MedController.downloadFile(imagePath, MedConstants.IMAGE_TYPE, messageID);
    }

    private void downloadQuesAudio(String audioPath, int messageID) {
        MedController.downloadFile(audioPath, MedConstants.AUDIO_TYPE, messageID);
    }

    private void downloadFile() {
        for (PatientQuestion item : mPatQuestionList) {
            if(item.mQuestionPictureUrl != null && !item.mQuestionPictureUrl.trim().equals("")) {
            	if (DEBUG) Log.d(TAG, "download picture: " + item.mQuestionPictureUrl);
            	mDownloadFileNum++;
                downloadQuesImage(item.mQuestionPictureUrl, item.mMessageID);
            }

            if(item.mQuestionWavUrl != null && !item.mQuestionWavUrl.trim().equals("")) {
            	if (DEBUG) Log.d(TAG, "download audio: " + item.mQuestionWavUrl);
            	mDownloadFileNum++;
                downloadQuesAudio(item.mQuestionWavUrl, item.mMessageID);
            }
        }
    }

    private void finishQuestionSubmit(boolean success) {
		Intent intent = new Intent();
		intent.setClass(this, MedAskQuestionFinish.class);
		intent.putExtra("success", success);
		startActivity(intent);
		pDialog.dismiss();
		dropQuestion();
		finish();
    }

    //implement the UI handler
    Handler mUiHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MedController.GET_QUESTION:
                if(msg.arg1 == 0) {
                	mSuccessItems++;
                    JSONObject jso = (JSONObject)msg.obj;
                    if (jso != null) {
                    	try{
                    		if(jso.getBoolean("success")) {
                    			JSONArray result = jso.getJSONArray("result");
                    			int length = result.length();
                    			for (int i = 0; i < length; i++) {
                    				JSONObject item = result.getJSONObject(i);
                    				String text = item.getString("question_desc");
                    				int attachType = item.getInt("attach_type");
                    				int questionID = item.getInt("question_id");
                    				int messageID = item.getInt("message_id");
                    				int direct = item.getInt("direct");
                    				String url = item.getString("attach_url");
                    				String imageFile = "", audioFile = "", previewFile = "";

                    				switch (attachType) {
                    				case MedConstants.IMAGE_TYPE:
                    					imageFile = url;
                    					audioFile = "";
                    					break;
                    				case MedConstants.AUDIO_TYPE:
                    					audioFile = url;
                    					imageFile = "";
                    					break;
                    				default:
                    					break;
                    				}
                    				addQuestionItem(imageFile, previewFile, audioFile, text, questionID, messageID, direct);
                    			}
                    		}
                            xHelper.log(jso.toString());
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}
                    	downloadFile();
                    	if (mDownloadFileNum <= 0) {
                        	pDialog.dismiss();
                    	}
                    }
                } else {
                	Log.e(TAG, "question read failed!");
                }
            	break;
            case MedController.DOWNLOAD_FILE:
            	if(msg.arg1 == 0) {
            		int msgID = msg.arg2;
            		//download file success.
            		for (PatientQuestion item : mPatQuestionList) {
            			if (item.mMessageID == msgID) {
            				String imageFile = MedUtil.getFileName(item.mQuestionPictureUrl);
            				// find the message which contains the downloaded file
            				if (imageFile != null && !imageFile.trim().equals("")) {
            					// this message contains the picture.
            					String previewPhotoPath = photoCompass(MedUtil.saveImageDir + imageFile, MAX_ICON_HEIGHT, MedConstants.PREVIEW);
            					item.mQuestionPicturePreviewUrl = previewPhotoPath;
            				}
            		        mlistfragment.update();
            			}
            		}
            	}
            	mDownloadFileNum --;
            	if (mDownloadFileNum <= 0) {
            		pDialog.dismiss();
            	}
            	break;
            case MedController.UPLOAD_FILE:
                //Medcontroller will sent this message back after it handled the request of uploading files.
            	mUploadFileNum--;
                Log.d(TAG, "mUploadFileNum = " + mUploadFileNum);
                if(msg.arg1 == 0) {
                	mSuccessFiles++;
                } else {
                	mFailedFiles++;
                }
                if (mUploadFileNum == 0) {
                	if (DEBUG) Log.d(TAG, "" + mSuccessFiles + " files upload success! " + mFailedFiles + " files upload failed!");
                	mSuccessFiles = 0;
                	mFailedFiles = 0;
                }
//              pDialog.dismiss();
                break;
            case MedController.ADD_QUESTION:
            	mQuestionItemNum--;
                Log.d(TAG, "mQuestionItemNum = " + mQuestionItemNum);
                if(msg.arg1 == 0) {
                	mSuccessItems++;
                    JSONObject jso = (JSONObject)msg.obj;
                    if (jso != null) {
                    	try{
                    		if(jso.getBoolean("success")) {
                    			if (mQuesID == 0) {
                    				// the first item of the question is inserted.
                        			JSONObject result = jso.getJSONObject("result");
                        			int index = result.getInt("index");
                        			int questionID = result.getInt("question_id");
                        			int messageID = result.getInt("message_id");
                    				mQuesID = questionID;
                        			PatientQuestion item = mPatQuestionList.get(index);
                        			if (item != null) {
                        				item.mQuestionID = questionID;
                        				item.mMessageID = messageID;
                        			}

                        			// set all the items question id to the new question got from server.
                        			for (PatientQuestion ques : mPatQuestionList) {
                        				ques.mQuestionID = mQuesID;
                        			}
                        			if (mPatQuestionList.size() > 1) {
                            			insertQuestion(mPatQuestionList);
                        			} else {
                        				mAskFinish = true;
                        				finishQuestionSubmit(true);
                        			}
                    			} else {
                    				// this new item is from a added question.
                        			JSONObject result = jso.getJSONObject("result");
                        			int index = result.getInt("index");
                        			int questionID = result.getInt("question_id");
                        			int messageID = result.getInt("message_id");
                        			PatientQuestion item = mPatQuestionList.get(index);
                        			if (item != null) {
                        				item.mQuestionID = questionID;
                        				item.mMessageID = messageID;
                        			}
                        			if (mQuestionItemNum == 0) {
                        				// all the items has been submitted
                        				finishQuestionSubmit(mFailedItems <= 0);
                        			}
                    			}
                    		}
                            xHelper.log(jso.toString());
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}
                    }
                } else {
                	mFailedItems++;
        			if (mQuestionItemNum == 0) {
        				// all the items has been submitted
        				// if last one failed.
        				finishQuestionSubmit(false);
        			}
                }
                if (mQuestionItemNum == 0) {
//                	Toast.makeText(MedBringupQuestion.this, "" + mSuccessItems + " items added success! " + mFailedItems + " items added failed!", Toast.LENGTH_LONG).show();
                	mSuccessItems = 0;
                	mFailedItems = 0;
                }
                break;
            default:
              	break;
            }
        }
    };

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		if (mQuestionText.getText().toString().trim().equals("")) {
			mFinishButton.setEnabled(false);
			mFinishButton.setAlpha(0.5f);
		} else {
			mFinishButton.setEnabled(true);
			mFinishButton.setAlpha(1.0f);
		}
	}
}
