package com.sixplus.med_sys_patient;

import org.json.JSONArray;
import org.json.JSONObject;

import us.xdroid.util.xHelper;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MedAddDoctorActivity extends Activity {
    static final String TAG = "MedAddDoctorActivity";
    final static boolean DEBUG = MedConstants.DEBUG;
	private ProgressDialog pDialog;
	private EditText mDocAuthEdit;
	private int mPatID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.med_add_doctor);
		
        //Must init the MedController here!!!
  	  	if(MedController.mController == null) {
  	  		MedController controller = new MedController(this);
  	  	}

  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);
  	  	
  	    mPatID = ((MedPatientApp)getApplicationContext()).getPatientID();

		((TextView)findViewById(R.id.navigation_title)).setText(R.string.add_doctor_title);

		mDocAuthEdit = (EditText)findViewById(R.id.doc_auth);
		TextView tx=(TextView)findViewById(R.id.submit);
        tx.setClickable(true);
        tx.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String docAuthNumber = mDocAuthEdit.getText().toString().trim();

				if (!validateDocAuth(docAuthNumber)) {
					String docAuthIncorrect = getResources().getString(R.string.doc_auth_incorrect);
					DialogUtil.showDialog(MedAddDoctorActivity.this, docAuthIncorrect, false);
					return;
				}

				MedController.addDoctor(mPatID, Integer.valueOf(docAuthNumber).intValue());

				String processing = getResources().getString(R.string.processing);
				//show progress dialog
				pDialog = new ProgressDialog(MedAddDoctorActivity.this);
				pDialog.setMessage(processing);
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(true);
				pDialog.show();
			}
		});
	}

	private boolean validateDocAuth(String docAuthNumber) {
		boolean result = false;
		if (docAuthNumber != null) {
			String numberString = docAuthNumber.trim();
			try {
				if (numberString.length() == 4 && Integer.valueOf(numberString) != null) {
					result = true;
				}
			} catch (NumberFormatException e) {
			}
		}
		return result;
	}

    //implement the UI handler
	Handler mUiHandler = new Handler() {
      @Override
      public void handleMessage(android.os.Message msg) {
          switch (msg.what) {
			case  MedController.ADD_DOCTOR:
		        pDialog.dismiss();
				if(msg.arg1 == 0) {
					if (msg.obj != null) {
						try{
							JSONObject jso = (JSONObject)msg.obj;
		            		if(jso.getBoolean("success")) {
                                finish();
		            		}
		            		else {
		            			String addDocFailed;
                    			JSONObject result = jso.getJSONObject("error");
                    			if (result != null) {
                    				String mesg = result.getString("message");
                    				if (mesg != null && mesg.trim().equals("Relation already exists")) {
                    					addDocFailed = getResources().getString(R.string.add_doc_already_exist);
                    				} else {
                    					addDocFailed = getResources().getString(R.string.add_doc_failed);
                    				}
                    				DialogUtil.showDialog(MedAddDoctorActivity.this, addDocFailed, false);
                    			}
		            		}
		                    xHelper.log(jso.toString());
		            	} catch (Exception e) {
		            		e.printStackTrace();
		            	}
					}
				} else {
					Toast.makeText(MedAddDoctorActivity.this, R.string.add_doc_failed, Toast.LENGTH_LONG).show();
				}
              break;
		  }
	   }
	};
}
