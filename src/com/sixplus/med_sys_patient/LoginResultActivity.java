package com.sixplus.med_sys_patient;

import org.json.JSONException;
import org.json.JSONObject;

import com.sixplus.med_sys_patient.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class LoginResultActivity extends Activity {
	private static final String TAG = "LoginResultActivity";
	private static boolean DEBUG = true;

	JSONObject mJSONObject = null;
	JSONObject mResultJSONObject = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_result);
        View mView = ((View)this.findViewById(android.R.id.content));
        TextView mResultTextView = (TextView)mView.findViewById(R.id.login_result);
        TextView mResultDetailTextView = (TextView)mView.findViewById(R.id.result_detail);
        
		Intent intent = getIntent();
		String response = intent.getStringExtra("response");
		if (DEBUG) Log.d(TAG, "response = " + response);
		try {
			mJSONObject = new JSONObject(response);
			if (mJSONObject != null) {
	            boolean loginResult = mJSONObject.getBoolean("success");
	            if (loginResult) {
	            	mResultTextView.setText("Login success");
		            mResultJSONObject = mJSONObject.getJSONObject("result").getJSONObject("login");
	            } else {
	            	mResultTextView.setText("Login failed");
		            mResultJSONObject = mJSONObject.getJSONObject("error");
	            }
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
        mResultDetailTextView.setText(mResultJSONObject.toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login_result, menu);
		return true;
	}

}
