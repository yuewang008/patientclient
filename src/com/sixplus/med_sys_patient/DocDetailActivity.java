package com.sixplus.med_sys_patient;

import org.json.JSONObject;

import us.xdroid.util.xHelper;

import com.sixplus.med_sys_patient.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class DocDetailActivity extends Activity {
    static final String TAG = "DocDetailActivity";
    final static boolean DEBUG = MedConstants.DEBUG;
    
	private int mDocID = 0;

    private ProgressDialog pDialog;
	private TextView mResumeTextview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.med_doctor_info_view);
		
		mDocID = ((MedPatientApp)getApplicationContext()).getDocID();
		
		((TextView)findViewById(R.id.navigation_title)).setText(R.string.clinic_info);
		
		View view = this.findViewById(android.R.id.content).getRootView();
		Intent intent = getIntent();
		TextView nameView = (TextView)view.findViewById(R.id.name);
		TextView roleView = (TextView)view.findViewById(R.id.title);
		TextView addressView = (TextView)view.findViewById(R.id.address);
		mResumeTextview = (TextView)view.findViewById(R.id.introduction);
		nameView.setText(intent.getStringExtra("name"));
		roleView.setText(intent.getStringExtra("role"));
		addressView.setText(intent.getStringExtra("address"));
		
  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);

    	//do the login in background
    	MedController.docDetail(mDocID);

		TextView tx=(TextView)findViewById(R.id.ask_problems);
        tx.setClickable(true);
        tx.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(DocDetailActivity.this, MedQuestionList.class);
				startActivity(intent);
			}
		});
        
		String processing = getResources().getString(R.string.processing);
		//show progress dialog
		pDialog = new ProgressDialog(DocDetailActivity.this);
		pDialog.setMessage(processing);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
    	mDocID = ((MedPatientApp)getApplicationContext()).getDocID();
    	//do the login in background
    	MedController.docDetail(mDocID);
	}

    //implement the UI handler
	Handler mUiHandler = new Handler() {
      @Override
      public void handleMessage(android.os.Message msg) {
          switch (msg.what) {
			case  MedController.DOC_DETAIL:
                //Medcontroller will sent this message back after it handled the request
				Log.d(TAG, "msg.arg1 = " + msg.arg1);
				if(msg.arg1 == 0) {
					if (msg.obj != null) {
						try{
							JSONObject jso = (JSONObject)msg.obj;
		            		if(jso.getBoolean("success")) {
		            			Log.d("login", "success");
		            			//result array with the returned rows 
		            			JSONObject result = jso.getJSONObject("result");
		            			String docDesc = result.getString("description");
		            			if (docDesc != null && !docDesc.trim().equals("")) {
			            			mResumeTextview.setText(docDesc);
		            			} else {
			            			mResumeTextview.setText("���޽���");
		            			}
		            		}
		            		else {
		            			Log.e(TAG, "find doctor detail failed");
		            		}
		                    xHelper.log(jso.toString());
		            	} catch (Exception e) {
		            		e.printStackTrace();
		            	}
					}
				} else {
					if (DEBUG) Log.d(TAG, "find doc detail failed!");
				}
				pDialog.dismiss();
              break;
		  }
	   }
	};
}
