package us.xdroid.util;
import android.os.Process;
import android.os.Message;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import us.xdroid.util.xHelper;
public class ControllerBase{
    protected static HandlerThread mThread;
    protected Context mContext;
    protected BackgroundHandler mBackgroundHandler;
    protected static Handler mUiHandler;
    protected static final Object sLock = new Object();
    public static void setUIHandler(Handler h) {
        synchronized (sLock) {
            mUiHandler = h;
        }
    }
	public void init(Context context){
		mContext = context;
        mThread = new HandlerThread("BasicController.Thread",
                                    Process.THREAD_PRIORITY_BACKGROUND);
        mThread.start();
        mBackgroundHandler = new BackgroundHandler(mThread.getLooper());
	}

	public Context getContext(){
		return mContext;
    }
	

	public void sendRequestNC(int reqid,int arg1,int arg2,Object obj){
		
        if (mBackgroundHandler!=null) mBackgroundHandler.sendMessage(mBackgroundHandler.obtainMessage(
                                       reqid,arg1,arg2,obj));
	}
	public void sendRequest(int reqid,int arg1,int arg2,Object obj){
		xHelper.log("sendRequest");
//        if (!mBackgroundHandler.hasMessages(reqid)) {
//			xHelper.log("sendRequest");
            mBackgroundHandler.sendMessage(mBackgroundHandler.obtainMessage(
                                               reqid,arg1,arg2,obj));
//        }
	}
	public void sendRequest(int reqid,Object obj){
		sendRequest(reqid,0,0,obj);
	}
	public void sendRequest(int reqid){
		sendRequest(reqid,0,0,null);
	}
	public void sendRequest(int reqid,int arg1, Object obj){
		sendRequest(reqid,arg1,0,obj);
	}
	public void sendRequest(int reqid,int arg1){
		sendRequest(reqid,arg1,0,null);
	}
	public void sendRequestNC(int reqid,int arg1){
		sendRequestNC(reqid,arg1,0,null);
	}
	public void sendRequestNC(int reqid,Object obj){
		sendRequestNC(reqid,0,0,obj);
	}
	public void sendRequestNC(int reqid){
		sendRequestNC(reqid,0,0,null);
	}
    public void sendMessageToUI(int msgid,int arg1,int arg2,Object obj) {
		if(mUiHandler!=null) mUiHandler.sendMessage(mUiHandler.obtainMessage(msgid,arg1,arg2,obj));
    }
    class BackgroundHandler extends Handler {
        BackgroundHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
			ControllerBase.this.handleMessage(msg);
		}
	}
	public void handleMessage(Message msg){
		
	}
}


	

